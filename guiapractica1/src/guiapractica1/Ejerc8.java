/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package guiapractica1;
import java.util.Scanner;
/**
 *
 * @author Kage Bushin No Jutsu
 */
public class Ejerc8 {
    public static void main(String[] args) {
        // Crear un objeto Scanner para leer la entrada del usuario
        Scanner scanner = new Scanner(System.in);

        // Pedir al usuario que ingrese la temperatura en grados Celsius
        System.out.print("Ingrese la temperatura en grados Celsius: ");
        double celsius = scanner.nextDouble();

        // Calcular la temperatura en Kelvin
        double kelvin = 273.15 + celsius;

        // Calcular la temperatura en grados Fahrenheit
        double fahrenheit = (1.8 * celsius) + 32;

        // Mostrar por pantalla la temperatura en Kelvin y grados Fahrenheit
        System.out.println("Temperatura en Kelvin: " + kelvin);
        System.out.println("Temperatura en grados Fahrenheit: " + fahrenheit);

        // Cerrar el objeto Scanner
        scanner.close();
    }
}
