/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package guiapractica1;
import java.util.Scanner;
/**
 *
 * @author Kage Bushin No Jutsu
 */
public class Ejerc6 {
    public static void main(String[] args) {
        // Crear un objeto Scanner para leer la entrada del usuario
        Scanner Scanner = new Scanner(System.in);

        // Pedir al usuario que ingrese el precio del producto
        System.out.print("Ingrese el precio del producto: ");
        double precio = Scanner.nextDouble();

        // Pedir al usuario que ingrese el porcentaje de descuento
        System.out.print("Ingrese el porcentaje de descuento: ");
        double descuento = Scanner.nextDouble();

        // Calcular el importe descontado
        double importeDescontado = precio * (descuento / 100);

        // Calcular el importe a pagar
        double importePagar = precio - importeDescontado;

        // Mostrar por pantalla el importe descontado y el importe a pagar
        System.out.println("Importe descontado: " + importeDescontado);
        System.out.println("Importe a pagar: " + importePagar);

        // Cerrar el objeto Scanner
        Scanner.close();
    }
}
