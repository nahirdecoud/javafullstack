/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package guiapractica1;
import java.util.Scanner;
/**
 *
 * @author Kage Bushin No Jutsu
 */
public class Ejerc7  {
    public static void main(String[] args) {
        // Crear un objeto Scanner para leer la entrada del usuario
        Scanner scanner = new Scanner(System.in);

        // Pedir al usuario que ingrese el valor de la primera edad
        System.out.print("Ingrese el valor de la primera edad: ");
        int edad1 = scanner.nextInt();

        // Pedir al usuario que ingrese el valor de la segunda edad
        System.out.print("Ingrese el valor de la segunda edad: ");
        int edad2 = scanner.nextInt();

        // Intercambiar los valores utilizando una variable auxiliar
        int auxiliar = edad1;
        edad1 = edad2;
        edad2 = auxiliar;

        // Mostrar por pantalla los valores intercambiados
        System.out.println("edad1 = " + edad1);
        System.out.println("edad2 = " + edad2);

        // Cerrar el objeto Scanner
        scanner.close(); 
    }
}
   
