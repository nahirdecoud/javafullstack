/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package guiapractica1;
import java.util.Scanner;
/**
 *
 * @author Kage Bushin No Jutsu
 */
public class Ejerc5 {
    public static void main(String[] args) {
        // Crear un objeto Scanner para leer la entrada del usuario
        Scanner scanner = new Scanner(System.in);

        // Pedir al usuario que ingrese el valor del radio
        System.out.print("Ingrese el valor del radio de la circunferencia: ");
        double radio = scanner.nextDouble();

        // Calcular el área y el perímetro utilizando las fórmulas
        double area = Math.PI * Math.pow(radio, 2);
        double perimetro = 2 * Math.PI * radio;

        // Mostrar por pantalla el resultado
        System.out.println("El área de la circunferencia es: " + area);
        System.out.println("El perímetro de la circunferencia es: " + perimetro);

        // Cerrar el objeto Scanner
        scanner.close();
    }
}
