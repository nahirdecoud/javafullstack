/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package guiapractica1;
import java.util.Scanner;
/**
 *
 * @author Kage Bushin No Jutsu
 */
public class Ejerc9 {
    public static void main(String[] args) {
        // Crear un objeto Scanner para leer la entrada del usuario
        Scanner scanner = new Scanner(System.in);

        // Pedir al usuario que ingrese la cantidad de pesos
        System.out.print("Ingrese la cantidad de pesos: ");
        double pesos = scanner.nextDouble();

        // Definir las tasas de conversión
        double tasaDolar = 231.68;
        double tasaEuro = 250.69;
        double tasaGuarani = 31.00;
        double tasaReal = 46.81;

        // Calcular las conversiones
        double dolares = pesos / tasaDolar;
        double euros = pesos / tasaEuro;
        double guaranies = pesos * tasaGuarani;
        double reales = pesos / tasaReal;

        // Mostrar por pantalla los resultados con dos decimales
        System.out.printf("Equivalente en dólares: %.2f%n", dolares);
        System.out.printf("Equivalente en euros: %.2f%n", euros);
        System.out.printf("Equivalente en guaraníes: %.2f%n", guaranies);
        System.out.printf("Equivalente en reales: %.2f%n", reales);

        // Cerrar el objeto Scanner
        scanner.close();
    }
}
